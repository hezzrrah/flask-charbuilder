# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 00:43:57 2018

@author: hextht
"""

import threading
import random
import asyncio

def just_print(loop):
    print("Another 3 seconds have passed...")
    loop.call_later(3, just_print, loop)
    
    
    
def main():
    loop = asyncio.get_event_loop()
    try:
        loop.call_soon(just_print, loop)
        loop.run_forever()
    finally:
        loop.close()
        
if __name__=="__main__":
    main()
